﻿using BuscadorDeCaracter.ConsoleApp.StreamTools;
using System;

namespace BuscadorDeCaracter.ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Input: ");
            var input = Console.ReadLine(); // "aAbBABacfe"

            var stream = new Stream(input);
            var fc = Stream.FirstChar(stream);

            if (fc == default(char))
                Console.WriteLine("Nenhum char encontrado!");
            else
                Console.WriteLine("Output: " + fc);

            Console.Read();
        }
    }
}
