﻿using System.Linq;
using System.Text.RegularExpressions;

namespace BuscadorDeCaracter.ConsoleApp.StreamTools
{
    /// <summary>
    /// Recebe uma string e possibilita percorrer através da mesma.
    /// </summary>
    public class Stream : IStream
    {
        /// <summary>
        /// String que será percorrida.
        /// </summary>
        public string Input { get; private set; }

        /// <summary>
        /// Determina a posição atual na string de entrada.
        /// </summary>
        private int posCurrent = 0;

        /// <summary>
        /// Construtor da classe, recebe uma string para iniciar a propriedade que será percorrida.
        /// </summary>
        /// <param name="input"></param>
        public Stream(string input)
        {
            Input = input;
        }

        /// <summary>
        /// Obtém o caracter atual e atualiza a posição.
        /// </summary>
        /// <returns>O caracter da posição atual.</returns>
        public char GetNext()
        {
            return Input[posCurrent++];
        }

        /// <summary>
        /// Se há caracteres ainda na posição atual da string de entrada.
        /// </summary>
        /// <returns>Um valor booleano dependendo se a string ainda tem posições a serem percorridas.</returns>
        public bool HasNext()
        {
            if (posCurrent <= Input.Length - 1)
                return true;
            return false;
        }

        /// <summary>
        /// Obtém a primeira vogal precedida por uma consoante e que não se repete.
        /// </summary>
        /// <param name="input">A stream com o string.</param>
        /// <returns>O primeiro caracter encontrado ou não.</returns>
        public static char FirstChar(IStream input)
        {
            string patternVowels = "[aeiuoAEIOU]";
            string patternConsonants = "[^aeiuoAEIOU]";
            string auxStr = "";
            string foundVowels = "";
            string ruleVowels = "";

            while (input.HasNext())
            {
                string letter = input.GetNext().ToString();
                auxStr = $"{auxStr}{letter}";

                if (auxStr.Length > 1)
                    auxStr = auxStr.Substring(auxStr.Length - 2, 2);

                if (Regex.IsMatch(letter, patternVowels))
                {
                    foundVowels += letter;

                    if (Regex.IsMatch(auxStr[0].ToString(), patternConsonants))
                    {
                        ruleVowels += letter;
                    }
                }
            }

            var first = ruleVowels.FirstOrDefault(x => foundVowels.Count(y => y == x) == 1);
            return first;
        }
    }
}
