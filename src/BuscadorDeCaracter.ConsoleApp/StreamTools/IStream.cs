﻿namespace BuscadorDeCaracter.ConsoleApp.StreamTools
{
    /// <summary>
    /// Recebe uma string e possibilita percorrer através da mesma.
    /// </summary>
    public interface IStream
    {
        /// <summary>
        /// String que será percorrida.
        /// </summary>
        string Input { get; }

        /// <summary>
        /// Obtém o caracter atual e atualiza a posição.
        /// </summary>
        /// <returns>O caracter da posição atual.</returns>
        char GetNext();

        /// <summary>
        /// Se há caracteres ainda na posição atual da string de entrada.
        /// </summary>
        /// <returns>Um valor booleano dependendo se a string ainda tem posições a serem percorridas.</returns>
        bool HasNext();
    }
}
