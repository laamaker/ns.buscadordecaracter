﻿using System;
using BuscadorDeCaracter.ConsoleApp.StreamTools;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BuscadorDeCaracter.Test.ConsoleApp
{
    [TestClass]
    public class StreamTest
    {
        [TestMethod]
        public void Should_Found_In_Mixed_Case()
        {
            IStream stream = new Stream("aAbBABacfe");
            Assert.AreEqual('e', Stream.FirstChar(stream));
        }

        [TestMethod]
        public void Should_Found_In_Uppercase()
        {
            IStream stream = new Stream("ABCDEFGHIJKLMNOP");
            Assert.AreEqual('E', Stream.FirstChar(stream));
        }

        [TestMethod]
        public void Should_Found_In_Lowercase()
        {
            IStream stream = new Stream("abcdefghijklmnop");
            Assert.AreEqual('e', Stream.FirstChar(stream));
        }

        [TestMethod]
        public void Should_Not_Found_In_Empty_String()
        {
            IStream stream = new Stream("");
            Assert.AreEqual(default(char), Stream.FirstChar(stream));
        }

        [TestMethod]
        public void Should_Not_Found_In_Vowels_Repeated()
        {
            IStream stream = new Stream("AbcDeFGTest");
            Assert.AreEqual(default(char), Stream.FirstChar(stream));
        }

        [TestMethod]
        public void Should_Not_Found_In_Consonants()
        {
            IStream stream = new Stream("bcDFGTstChrctR");
            Assert.AreEqual(default(char), Stream.FirstChar(stream));
        }
    }
}
